package lfs

import (
	"log"
	"time"

	"gitlab.com/ilmikko/lfs/config"
	"gitlab.com/ilmikko/lfs/lfs/mirror"
)

type LFS struct {
	CFG     *config.Config
	Mirrors []*mirror.Mirror
}

func (l *LFS) Watch() error {
	log.Printf("Watching %d mirror(s) with %v interval...", len(l.Mirrors), l.CFG.WatchInterval)
	for {
		for _, m := range l.Mirrors {
			if err := m.WatchTick(); err != nil {
				log.Printf("watch: %v", err)
			}
		}

		time.Sleep(l.CFG.WatchInterval)
	}
}

func New(cfg *config.Config) (*LFS, error) {
	l := &LFS{}

	l.CFG = cfg
	l.Mirrors = []*mirror.Mirror{}

	for _, cm := range cfg.Mirrors {
		m, err := mirror.New(cm)
		if err != nil {
			return nil, err
		}

		l.Mirrors = append(l.Mirrors, m)
	}

	return l, nil
}
