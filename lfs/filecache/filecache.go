package filecache

import (
	"os"
	"path/filepath"
	"time"

	"gitlab.com/ilmikko/lfs/lfs/meta"
	"gitlab.com/ilmikko/lfs/lfs/path"
)

type FileCache struct {
	cache map[string]time.Time
}

func (fc *FileCache) changedFilesRecursive(rp string) ([]*meta.Meta, error) {
	stat, err := os.Stat(rp)
	if err != nil {
		return nil, err
	}

	if !stat.IsDir() {
		mt := stat.ModTime()

		if fc.FileSeen(rp, mt) {
			return []*meta.Meta{}, nil
		}

		m, err := meta.Create(rp)
		if err != nil {
			return nil, err
		}

		return []*meta.Meta{m}, nil
	}

	files, err := path.LsDir(rp)
	if err != nil {
		return nil, err
	}

	metas := []*meta.Meta{}
	for _, name := range files {
		m, err := fc.changedFilesRecursive(
			filepath.Join(rp, name),
		)
		if err != nil {
			return nil, err
		}

		metas = append(metas, m...)
	}
	return metas, nil
}

func (fc *FileCache) ChangedFiles(base string) ([]*meta.Meta, error) {
	metas, err := fc.changedFilesRecursive(base)
	if err != nil {
		return nil, err
	}

	// Trim meta file paths to remove the base.
	for _, m := range metas {
		if err := m.RemoveBase(base); err != nil {
			return nil, err
		}
	}

	return metas, nil
}

func (fc *FileCache) FileUpdate(p string, nt time.Time) {
	fc.cache[p] = nt
}

func (fc *FileCache) FileSeen(p string, nt time.Time) bool {
	ot, ok := fc.cache[p]
	if !ok {
		return false
	}

	if nt.After(ot) {
		return false
	}

	return true
}

func (fc *FileCache) Update(p string) error {
	metas, err := fc.changedFilesRecursive(p)
	if err != nil {
		return err
	}

	for _, m := range metas {
		fc.FileUpdate(m.File, m.ModTime)
	}
	return nil
}

func New() *FileCache {
	return &FileCache{
		cache: map[string]time.Time{},
	}
}
