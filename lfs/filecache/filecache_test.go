package filecache_test

import (
	"testing"
	"time"

	"gitlab.com/ilmikko/lfs/lfs/filecache"
)

func TestFileSeen(t *testing.T) {

	fc := filecache.New()
	ot := time.Time{}

	// Unseen files are not seen
	{
		want := false
		got := fc.FileSeen("/", ot)
		if want != got {
			t.Errorf("FileSeen not as expected: \n got: %v\nwant: %v", got, want)
		}
	}

	// Old files are seen
	fc.FileUpdate("/", ot)
	{
		want := true
		got := fc.FileSeen("/", ot)
		if want != got {
			t.Errorf("FileSeen not as expected: \n got: %v\nwant: %v", got, want)
		}
	}

	// Very old files are seen as well
	nt := ot.Add(1 * time.Minute)
	fc.FileUpdate("/", nt)
	{
		want := true
		got := fc.FileSeen("/", ot)
		if want != got {
			t.Errorf("FileSeen not as expected: \n got: %v\nwant: %v", got, want)
		}
	}

	// But new files are not seen
	fc.FileUpdate("/", ot)
	{
		want := false
		got := fc.FileSeen("/", nt)
		if want != got {
			t.Errorf("FileSeen not as expected: \n got: %v\nwant: %v", got, want)
		}
	}
}
