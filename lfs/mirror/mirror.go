package mirror

import (
	"fmt"

	"gitlab.com/ilmikko/lfs/config"
	"gitlab.com/ilmikko/lfs/lfs/filecache"
	"gitlab.com/ilmikko/lfs/lfs/path"
)

type Mirror struct {
	ID      string
	Mirrors []*config.Projection
	Files   struct {
		In  []*config.Projection
		Out []*config.Projection
	}
	linkCache *filecache.FileCache
	fileCache *filecache.FileCache
}

func New(cm *config.Mirror) (*Mirror, error) {
	if !path.IsDir(cm.Id) {
		return nil, fmt.Errorf("Expected %q to be a directory but it was not!", cm.Id)
	}

	m := &Mirror{
		ID:        cm.Id,
		linkCache: filecache.New(),
		fileCache: filecache.New(),
	}

	if err := path.CreateDirectory(m.PathDataDir()); err != nil {
		return nil, err
	}
	if err := path.CreateDirectory(m.PathMetaDir()); err != nil {
		return nil, err
	}

	m.Mirrors = append(m.Mirrors, cm.Links...)
	m.Files.Out = append(m.Files.Out, cm.Files.Out...)

	for _, l := range cm.Files.In {
		m.Files.In = append(m.Files.In, l)
		// Update the file cache so files won't get sent over until they change.
		if err := m.fileCache.Update(l.Dir); err != nil {
			return nil, err
		}
	}

	return m, nil
}
