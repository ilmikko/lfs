package mirror

import (
	"fmt"
	"log"
	"path/filepath"
	"time"

	"gitlab.com/ilmikko/lfs/lfs/meta"
	"gitlab.com/ilmikko/lfs/lfs/path"
)

func (m *Mirror) FileInTick() error {
	for _, p := range m.Files.In {
		metas, err := m.fileCache.ChangedFiles(p.Dir)
		if err != nil {
			return err
		}

		for _, mt := range metas {
			if p.IsIgnored(mt.File) {
				continue
			}
			if err := m.FileInUpdate(p.Dir, mt); err != nil {
				return err
			}
		}
	}
	return nil
}

func (m *Mirror) FileInUpdate(dir string, mt *meta.Meta) error {
	fileSrc := filepath.Join(dir, mt.File)
	name := path.Escape(mt.File)

	log.Printf("%s -> %s:%s", fileSrc, m.ID, name)

	// Copy file over to data path, write metadata (modification time) to meta after copying
	// This is so that in partial copies, lack of metadata prevents the file to be copied prematurely
	if err := path.CopyFile(
		fileSrc,
		m.PathData(name),
	); err != nil {
		return fmt.Errorf("file copy: %v", err)
	}

	if err := mt.Save(m.PathMeta(name)); err != nil {
		return fmt.Errorf("meta save: %v", err)
	}

	m.fileCache.FileUpdate(fileSrc, mt.ModTime)
	return nil
}

func (m *Mirror) fileUpdated(dest string, mt *meta.Meta) (bool, error) {
	destMod, err := path.ModTime(dest)
	if err != nil {
		return false, fmt.Errorf("cannot get data mod time: %v", err)
	}
	srcMod := mt.ModTime

	if !srcMod.After(destMod) {
		return false, nil
	}

	return true, nil
}

func (m *Mirror) FileOutUpdate(mt *meta.Meta) error {
	for _, p := range m.Files.Out {
		if p.IsIgnored(mt.File) {
			continue
		}

		dest := filepath.Join(p.Dir, mt.File)
		data := m.PathData(mt.ID)

		updated, err := m.fileUpdated(dest, mt)
		if err != nil {
			return fmt.Errorf("cannot determine update status: %v", err)
		}
		if !updated {
			continue
		}

		log.Printf("%s:%s -> %s", m.ID, mt.ID, dest)
		{
			now := time.Now()
			log.Printf("Propagation stats: Delay %s, Transit %s, Pickup %s", now.Sub(mt.ModTime), now.Sub(mt.DiscoveryTime), mt.DiscoveryTime.Sub(mt.ModTime))
		}
		if err := path.CreateDirectoryStructure(dest); err != nil {
			return fmt.Errorf("directory structure creation: %v", err)
		}

		if err := path.CopyFile(data, dest); err != nil {
			return fmt.Errorf("file copy: %v", err)
		}

		// Mod time is updated here so that this file does not get copied needlessly.
		// TODO: This doesn't work in case there are multiple LFS processes running at the same time.
		// Maybe we should save metadata of the ends as well?
		mt, err := path.ModTime(dest)
		if err != nil {
			return err
		}
		m.fileCache.FileUpdate(dest, mt)
	}
	return nil
}
