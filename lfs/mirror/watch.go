package mirror

import (
	"fmt"
	"log"

	"gitlab.com/ilmikko/lfs/lfs/meta"
	"gitlab.com/ilmikko/lfs/lfs/path"
)

func (m *Mirror) remove(mt *meta.Meta) error {
	meta := m.PathMeta(mt.ID)
	data := m.PathData(mt.ID)

	if err := path.Remove(meta); err != nil {
		return err
	}
	if err := path.Remove(data); err != nil {
		return err
	}
	return nil
}

func (m *Mirror) WatchTick() error {
	if err := m.FileInTick(); err != nil {
		log.Printf("file in tick: %v", err)
	}

	md := m.PathMetaDir()

	// We loop through the meta files, as this is the last file that will be written.
	// This way we can have the file transmission atomic, as large files may exist in the file folder,
	// but as the meta file has not been written, we will not touch them yet.
	files, err := path.LsDir(md)
	if err != nil {
		return err
	}

	for _, name := range files {
		mt, err := meta.LoadFile(md, name)
		if err != nil {
			return fmt.Errorf("unable to load meta file %q: %v", name, err)
		}

		if err := m.FileOutUpdate(mt); err != nil {
			return fmt.Errorf("project update for file %q: %v", name, err)
		}
		if err := m.LinkUpdate(mt); err != nil {
			return fmt.Errorf("link update for file %q: %v", name, err)
		}
		if err := m.remove(mt); err != nil {
			return fmt.Errorf("removal of file %q: %v", name, err)
		}
	}

	return nil
}
