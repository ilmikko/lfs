package mirror

import (
	"path/filepath"
)

const (
	DATA_IDENTIFIER = "data"
	META_IDENTIFIER = "meta"
)

func (m *Mirror) Path(id, name string) string {
	return filepath.Join(m.ID, id, name)
}

func (m *Mirror) PathMeta(name string) string {
	return m.Path(META_IDENTIFIER, name)
}

func (m *Mirror) PathData(name string) string {
	return m.Path(DATA_IDENTIFIER, name)
}

func (m *Mirror) PathOther(other, id, name string) string {
	return filepath.Join(other, id, name)
}

func (m *Mirror) PathMetaOther(other, name string) string {
	return m.PathOther(other, META_IDENTIFIER, name)
}

func (m *Mirror) PathDataOther(other, name string) string {
	return m.PathOther(other, DATA_IDENTIFIER, name)
}

func (m *Mirror) PathMetaDir() string {
	return filepath.Join(m.ID, META_IDENTIFIER)
}

func (m *Mirror) PathDataDir() string {
	return filepath.Join(m.ID, DATA_IDENTIFIER)
}
