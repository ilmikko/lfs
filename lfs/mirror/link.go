package mirror

import (
	"fmt"

	"gitlab.com/ilmikko/lfs/lfs/meta"
	"gitlab.com/ilmikko/lfs/lfs/path"
)

func (m *Mirror) LinkUpdate(mt *meta.Meta) error {
	metaSrc := m.PathMeta(mt.ID)
	dataSrc := m.PathData(mt.ID)

	// If the link has already seen this file, we won't propagate it the second time.
	if seen := m.linkCache.FileSeen(dataSrc, mt.ModTime); seen {
		return nil
	}

	for _, p := range m.Mirrors {
		if p.IsIgnored(mt.File) {
			continue
		}

		// log.Printf("%s:%s -> %s:%s", m.ID, mt.ID, p.Dir, mt.ID)

		metaDst := m.PathMetaOther(p.Dir, mt.ID)
		dataDst := m.PathDataOther(p.Dir, mt.ID)

		if err := path.CreateDirectoryStructure(metaDst); err != nil {
			return fmt.Errorf("meta directory: %v", err)
		}
		if err := path.CreateDirectoryStructure(dataDst); err != nil {
			return fmt.Errorf("data directory: %v", err)
		}

		if err := path.CopyFile(dataSrc, dataDst); err != nil {
			return fmt.Errorf("data copy: %v", err)
		}
		if err := path.CopyFile(metaSrc, metaDst); err != nil {
			return fmt.Errorf("meta copy: %v", err)
		}
	}

	m.linkCache.FileUpdate(dataSrc, mt.ModTime)
	return nil
}
