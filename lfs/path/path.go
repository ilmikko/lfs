package path

import (
	"fmt"
	"io"
	"log"
	"net/url"
	"os"
	"path/filepath"
	"time"
)

const (
	DIRMODE = 0755
)

func CopyFile(s, d string) error {
	out, err := os.Create(d)
	if err != nil {
		return fmt.Errorf("os.Create(%q): %v", d, err)
	}
	defer out.Close()

	in, err := os.Open(s)
	if err != nil {
		return fmt.Errorf("os.Open(%q): %v", s, err)
	}
	defer in.Close()

	if _, err := io.Copy(out, in); err != nil {
		return fmt.Errorf("io.Copy: %v", err)
	}

	if err := out.Close(); err != nil {
		return fmt.Errorf("out.Close: %v", err)
	}
	return nil
}

func CreateDirectory(path string) error {
	if err := os.MkdirAll(path, DIRMODE); err != nil {
		return fmt.Errorf("os.MkdirAll(%q): %v", path, err)
	}
	return nil
}

func CreateDirectoryStructure(path string) error {
	d := filepath.Dir(path)
	if err := CreateDirectory(d); err != nil {
		return err
	}
	return nil
}

func Escape(path string) string {
	return url.PathEscape(path)
}

func IsDir(path string) bool {
	s, err := os.Stat(path)
	if err != nil {
		return false
	}

	if !s.IsDir() {
		return false
	}

	return true
}

func LsDir(path string) ([]string, error) {
	dir, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer dir.Close()

	names, err := dir.Readdirnames(0)
	if err != nil {
		return nil, fmt.Errorf("Readdirnames: %v", err)
	}
	return names, nil
}

func ModTime(file string) (time.Time, error) {
	stat, err := os.Stat(file)
	if err != nil {
		if os.IsNotExist(err) {
			return time.Time{}, nil
		}

		return time.Time{}, err
	}

	return stat.ModTime(), nil
}

func Remove(path string) error {
	if err := os.Remove(path); err != nil {
		// Ignore IsNotExist as technically it's removed successfully.
		if os.IsNotExist(err) {
			return nil
		}
		return err
	}
	return nil
}

func Unescape(path string) string {
	p, err := url.PathUnescape(path)
	if err != nil {
		log.Fatalf("url.PathUnescape(%q): %v", path, err)
	}
	return p
}
