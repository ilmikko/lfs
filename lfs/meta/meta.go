package meta

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"gitlab.com/ilmikko/lfs/lfs/path"
)

type Meta struct {
	ID   string
	File string

	ModTime       time.Time
	DiscoveryTime time.Time
}

func (m *Meta) RemoveBase(base string) error {
	trim, err := filepath.Rel(base, m.File)
	if err != nil {
		return err
	}

	m.SetFile(trim)
	return nil
}

func (m *Meta) Save(meta string) error {
	out, err := os.Create(meta)
	if err != nil {
		return err
	}
	defer out.Close()

	fmt.Fprintf(out, "DT: %d\n", m.DiscoveryTime.UnixNano())
	fmt.Fprintf(out, "MT: %d\n", m.ModTime.UnixNano())
	return nil
}

func (m *Meta) SetFile(file string) {
	m.File = file
	m.ID = path.Escape(file)
}

func LoadFile(dir, file string) (*Meta, error) {
	f, err := os.Open(filepath.Join(dir, file))
	if err != nil {
		return nil, err
	}
	defer f.Close()

	m := New(file)

	s := bufio.NewScanner(f)
	for s.Scan() {
		fields := strings.Fields(s.Text())
		key, fields := fields[0], fields[1:]
		switch key := strings.TrimSuffix(key, ":"); key {
		case "DT":
			t, err := strconv.ParseInt(
				fields[0],
				10, 64)
			if err != nil {
				return nil, err
			}
			m.DiscoveryTime = time.Unix(0, t)
		case "MT":
			t, err := strconv.ParseInt(
				fields[0],
				10, 64)
			if err != nil {
				return nil, err
			}
			m.ModTime = time.Unix(0, t)
		default:
			return nil, fmt.Errorf("Unknown field identifier in meta file: %q", key)
		}
	}

	return m, nil
}

func Create(file string) (*Meta, error) {
	m := New(path.Escape(file))

	stat, err := os.Stat(file)
	if err != nil {
		return nil, err
	}

	m.ModTime = stat.ModTime()
	m.DiscoveryTime = time.Now()

	return m, nil
}

func New(id string) *Meta {
	m := &Meta{}

	m.ID = id
	m.File = path.Unescape(id)

	return m
}
