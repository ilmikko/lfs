package meta_test

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"reflect"
	"testing"
	"time"

	"gitlab.com/ilmikko/lfs/lfs/meta"
)

func TestLoad(t *testing.T) {
	dir, err := ioutil.TempDir("", "Test")
	if err != nil {
		t.Fatalf("Cannot create temp dir: %v", err)
	}
	defer os.RemoveAll(dir)

	mf := filepath.Join(dir, "meta%2Dfile")

	data := []byte("DT: 1586584314000000000\nMT: 1586584304000000000\n")

	if err := ioutil.WriteFile(mf, data, 0644); err != nil {
		t.Fatalf("WriteFile: %v", err)
	}

	m, err := meta.LoadFile(dir, "meta%2Dfile")
	if err != nil {
		t.Errorf("LoadFile: %v", err)
	}

	{
		got := m
		want := &meta.Meta{
			ID:            "meta%2Dfile",
			File:          "meta-file",
			ModTime:       time.Unix(1586584304, 0),
			DiscoveryTime: time.Unix(1586584314, 0),
		}

		if !reflect.DeepEqual(got, want) {
			t.Errorf("Metadata not as expected: \n got: %+v\nwant: %+v", got, want)
		}
	}
}

func TestSave(t *testing.T) {
	dir, err := ioutil.TempDir("", "Test")
	if err != nil {
		t.Fatalf("Cannot create temp dir: %v", err)
	}
	defer os.RemoveAll(dir)

	df := filepath.Join(dir, "/datafile")
	mf := filepath.Join(dir, "/metafile")

	m := meta.New(df)

	m.DiscoveryTime = time.Unix(1586584314, 0)
	m.ModTime = time.Unix(1586584304, 0)

	if err := m.Save(mf); err != nil {
		t.Errorf("Save: %v", err)
	}

	got, err := ioutil.ReadFile(mf)
	if err != nil {
		t.Errorf("ReadFile: %v", err)
	}

	{
		got := string(got)
		want := "DT: 1586584314000000000\nMT: 1586584304000000000\n"
		if got != want {
			t.Errorf("Meta file not as expected: \n got: %q\nwant: %q", got, want)
		}
	}
}
