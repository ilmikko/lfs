.PHONY: test

GITPATH=gitlab.com/ilmikko/lfs

PACKAGES=./config \
				 ./config/parser \
				 ./lfs \
				 ./lfs/filecache \
				 ./lfs/meta \
				 ./lfs/mirror \
				 ./lfs/path \

# We fake a GOPATH for people that just want to build the module.
build:
	mkdir -p /tmp/go/src/$(shell dirname $(GITPATH))
	test -L /tmp/go/src/$(GITPATH) || ln -sf $(shell pwd) /tmp/go/src/$(GITPATH)
	GOPATH="/tmp/go" go build -o bin/lfs lfs.go
	@rm -rf /tmp/go

test:
	go test $(PACKAGES)
