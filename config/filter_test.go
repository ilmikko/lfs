package config_test

import (
	"fmt"
	"reflect"
	"testing"

	"gitlab.com/ilmikko/lfs/config"
)

func TestAllow(t *testing.T) {
	f := config.NewFilter("Filter")

	testData := []string{
		"/test",
		"/test/test",
		"/test1",
		"/test1/test",
		"/test1/test2",
		"/test2/test",
		"/test2/test/test",
		"/test2/test/test3",
		"/test2/test2",
		"/test2/test2/test3",
	}

	testCases := []struct {
		allow   string
		want    []string
		wantErr error
	}{
		{
			allow: "^/$",
			want:  []string{},
		},
		{
			allow: "^/test2/test/",
			want:  []string{"/test2/test/test", "/test2/test/test3"},
		},
		{
			allow: "^/test1",
			want: []string{
				"/test1", "/test1/test", "/test1/test2",
				"/test2/test/test", "/test2/test/test3",
			},
		},
		{
			allow: "3",
			want: []string{
				"/test1", "/test1/test", "/test1/test2",
				"/test2/test/test", "/test2/test/test3",
				"/test2/test2/test3",
			},
		},
		{
			allow:   "[[[[[",
			wantErr: fmt.Errorf("error parsing regexp: missing closing ]: `[[[[[`"),
			want: []string{
				"/test1", "/test1/test", "/test1/test2",
				"/test2/test/test", "/test2/test/test3",
				"/test2/test2/test3",
			},
		},
	}

	for _, tc := range testCases {
		err := f.Allow(tc.allow)
		if tc.wantErr != nil {
			if err == nil {
				t.Errorf("Expected an error %q but was nil!", tc.wantErr.Error())
			} else {
				got := err.Error()
				want := tc.wantErr.Error()
				if got != want {
					t.Errorf("Error not as expected: \n got: %q\nwant: %q", got, want)
				}
			}
		} else {
			if err != nil {
				t.Errorf("Allow: %v", err)
			}
		}
		got := []string{}
		for _, td := range testData {
			if !f.Matches(td) {
				continue
			}
			got = append(got, td)
		}
		want := tc.want
		if !reflect.DeepEqual(got, want) {
			t.Errorf("Filtered data not as expected: \n got: %q\nwant: %q", got, want)
		}
	}
}

func TestEmptyMatchesNone(t *testing.T) {
	f := config.NewFilter("Filter")

	testCases := []string{"", "/", "/file", "/folder/file", "//folder"}

	for _, tc := range testCases {
		if f.Matches(tc) != false {
			t.Errorf("Case %q returns true for empty filter!", tc)
		}
	}
}
