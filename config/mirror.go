package config

type Mirror struct {
	Files struct {
		In  []*Projection
		Out []*Projection
	}
	Id    string
	Links []*Projection
}

func NewMirror(id string) *Mirror {
	m := &Mirror{}

	m.Id = id
	m.Files.In = []*Projection{}
	m.Files.Out = []*Projection{}
	m.Links = []*Projection{}

	return m
}
