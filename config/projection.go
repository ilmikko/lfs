package config

import (
	"fmt"
	"path/filepath"

	"gitlab.com/ilmikko/lfs/lfs/path"
)

type Projection struct {
	Filter *Filter
	Dir    string
}

func (p *Projection) IsIgnored(name string) bool {
	if p.Filter == nil {
		return false
	}

	if p.Filter.Matches(name) {
		return false
	}

	return true
}

func NewProjection(dir string) (*Projection, error) {
	p := &Projection{}

	dir = filepath.Clean(dir)
	if !path.IsDir(dir) {
		return nil, fmt.Errorf("Expected %q to be a directory but it was not!", dir)
	}
	p.Dir = dir

	return p, nil
}
