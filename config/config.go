package config

import (
	"fmt"
	"path/filepath"
	"time"

	"gitlab.com/ilmikko/lfs/config/parser"
)

const (
	DEFAULT_WATCH_INTERVAL = 5 * time.Second
)

type Config struct {
	Filters       map[string]*Filter
	Mirrors       map[string]*Mirror
	WatchInterval time.Duration
	parser        *parser.Parser
}

func (cfg *Config) Filter(id string) (*Filter, error) {
	f, ok := cfg.Filters[id]
	if !ok {
		return nil, fmt.Errorf("Unknown filter: %q", id)
	}
	return f, nil
}

func (cfg *Config) Mirror(id string) (*Mirror, error) {
	id = filepath.Clean(id)
	m, ok := cfg.Mirrors[id]
	if !ok {
		return nil, fmt.Errorf("Unknown mirror: %q", id)
	}
	return m, nil
}

func New() (*Config, error) {
	cfg := &Config{}

	cfg.parser = parser.New()

	cfg.Filters = map[string]*Filter{}
	cfg.Mirrors = map[string]*Mirror{}
	cfg.WatchInterval = DEFAULT_WATCH_INTERVAL

	if err := cfg.parseFlags(); err != nil {
		return nil, err
	}

	return cfg, nil
}
