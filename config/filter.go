package config

import (
	"regexp"
	"strings"
)

type Filter struct {
	Id       string
	Regexp   *regexp.Regexp
	original []string
}

func (f *Filter) Allow(s string) error {
	f.original = append(f.original, s)

	r, err := regexp.Compile(
		strings.Join(f.original, "|"),
	)
	if err != nil {
		return err
	}

	f.Regexp = r
	return nil
}

func (f *Filter) Matches(s string) bool {
	// Disallow by default.
	if f.Regexp == nil {
		return false
	}

	return f.Regexp.MatchString(s)
}

func NewFilter(id string) *Filter {
	f := &Filter{}

	f.Id = id
	f.original = []string{}

	return f
}
