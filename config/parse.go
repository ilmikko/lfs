package config

import (
	"fmt"
	"path/filepath"
	"time"

	"gitlab.com/ilmikko/lfs/lfs/path"
)

type Opts map[string]func([]string) error

func (cfg *Config) Switch(fields []string, opts Opts) error {
	if err := cfg.parser.Left(fields); err != nil {
		return err
	}
	cmd, fields := fields[0], fields[1:]
	f, ok := opts[cmd]
	if !ok {
		if err := cfg.parser.Unknown(cmd); err != nil {
			return err
		}
	}
	if err := f(fields); err != nil {
		return fmt.Errorf("%s: %v", cmd, err)
	}
	return nil
}

func (cfg *Config) mirrorProj(mirror, filter, dir string) (*Mirror, *Projection, error) {
	m, err := cfg.Mirror(mirror)
	if err != nil {
		return nil, nil, err
	}

	p, err := NewProjection(dir)
	if err != nil {
		return nil, nil, err
	}

	if filter != "" {
		f, err := cfg.Filter(filter)
		if err != nil {
			return nil, nil, err
		}
		p.Filter = f
	}

	return m, p, nil
}

func (cfg *Config) pFileInAll(fields []string) error {
	id, dir, err := cfg.parser.Grab2(fields)
	if err != nil {
		return err
	}

	m, p, err := cfg.mirrorProj(id, "", dir)
	if err != nil {
		return err
	}

	m.Files.In = append(m.Files.In, p)
	return nil
}

func (cfg *Config) pFileInFilter(fields []string) error {
	filter, mirror, dir, err := cfg.parser.Grab3(fields)
	if err != nil {
		return err
	}

	m, p, err := cfg.mirrorProj(mirror, filter, dir)
	if err != nil {
		return err
	}

	m.Files.In = append(m.Files.In, p)
	return nil
}

func (cfg *Config) pFileIn(fields []string) error {
	if err := cfg.Switch(fields, Opts{
		"ALL":    cfg.pFileInAll,
		"FILTER": cfg.pFileInFilter,
	}); err != nil {
		return err
	}
	return nil
}

func (cfg *Config) pFileOutAll(fields []string) error {
	id, dir, err := cfg.parser.Grab2(fields)
	if err != nil {
		return err
	}

	m, p, err := cfg.mirrorProj(id, "", dir)
	if err != nil {
		return err
	}

	m.Files.Out = append(m.Files.Out, p)
	return nil
}

func (cfg *Config) pFileOutFilter(fields []string) error {
	filter, mirror, dir, err := cfg.parser.Grab3(fields)
	if err != nil {
		return err
	}

	m, p, err := cfg.mirrorProj(mirror, filter, dir)
	if err != nil {
		return err
	}

	m.Files.Out = append(m.Files.Out, p)
	return nil
}

func (cfg *Config) pFileOut(fields []string) error {
	if err := cfg.Switch(fields, Opts{
		"ALL":    cfg.pFileOutAll,
		"FILTER": cfg.pFileOutFilter,
	}); err != nil {
		return err
	}
	return nil
}

func (cfg *Config) pFileSyncAll(fields []string) error {
	id, dir, err := cfg.parser.Grab2(fields)
	if err != nil {
		return err
	}

	m, p, err := cfg.mirrorProj(id, "", dir)
	if err != nil {
		return err
	}

	m.Files.In = append(m.Files.In, p)
	m.Files.Out = append(m.Files.Out, p)
	return nil
}

func (cfg *Config) pFileSyncFilter(fields []string) error {
	filter, mirror, dir, err := cfg.parser.Grab3(fields)
	if err != nil {
		return err
	}

	m, p, err := cfg.mirrorProj(mirror, filter, dir)
	if err != nil {
		return err
	}

	m.Files.In = append(m.Files.In, p)
	m.Files.Out = append(m.Files.Out, p)
	return nil
}

func (cfg *Config) pFileSync(fields []string) error {
	if err := cfg.Switch(fields, Opts{
		"ALL":    cfg.pFileSyncAll,
		"FILTER": cfg.pFileSyncFilter,
	}); err != nil {
		return err
	}
	return nil
}

func (cfg *Config) pFile(fields []string) error {
	if err := cfg.Switch(fields, Opts{
		"IN":   cfg.pFileIn,
		"OUT":  cfg.pFileOut,
		"SYNC": cfg.pFileSync,
	}); err != nil {
		return err
	}
	return nil
}

func (cfg *Config) pFilterAllow(fields []string) error {
	id, filter, err := cfg.parser.Grab2(fields)
	if err != nil {
		return err
	}

	f, err := cfg.Filter(id)
	if err != nil {
		return err
	}

	if err := f.Allow(filter); err != nil {
		return err
	}
	return nil
}

func (cfg *Config) pFilterCreate(fields []string) error {
	id, err := cfg.parser.GrabAll(fields)
	if err != nil {
		return err
	}

	cfg.Filters[id] = NewFilter(id)
	return nil
}

func (cfg *Config) pFilter(fields []string) error {
	if err := cfg.Switch(fields, Opts{
		"ALLOW":  cfg.pFilterAllow,
		"CREATE": cfg.pFilterCreate,
	}); err != nil {
		return err
	}
	return nil
}

func (cfg *Config) pInterval(fields []string) error {
	ds, err := cfg.parser.GrabAll(fields)
	if err != nil {
		return err
	}

	d, err := time.ParseDuration(ds)
	if err != nil {
		return err
	}

	cfg.WatchInterval = d
	return nil
}

func (cfg *Config) pLinkAll(fields []string) error {
	id, dir, err := cfg.parser.Grab2(fields)
	if err != nil {
		return err
	}

	m, p, err := cfg.mirrorProj(id, "", dir)
	if err != nil {
		return err
	}

	m.Links = append(m.Links, p)
	return nil
}

func (cfg *Config) pLinkFilter(fields []string) error {
	filter, mirror, dir, err := cfg.parser.Grab3(fields)
	if err != nil {
		return err
	}

	m, p, err := cfg.mirrorProj(mirror, filter, dir)
	if err != nil {
		return err
	}

	m.Links = append(m.Links, p)
	return nil
}

func (cfg *Config) pLink(fields []string) error {
	if err := cfg.Switch(fields, Opts{
		"ALL":    cfg.pLinkAll,
		"FILTER": cfg.pLinkFilter,
	}); err != nil {
		return err
	}
	return nil
}

func (cfg *Config) pMirror(fields []string) error {
	id, err := cfg.parser.GrabAll(fields)
	if err != nil {
		return err
	}

	id = filepath.Clean(id)
	if !path.IsDir(id) {
		return fmt.Errorf("Expected %q to be a directory but it was not!", id)
	}

	cfg.Mirrors[id] = NewMirror(id)
	return nil
}

func (cfg *Config) p(fields []string) error {
	if err := cfg.Switch(fields, Opts{
		"FILE":     cfg.pFile,
		"FILTER":   cfg.pFilter,
		"INTERVAL": cfg.pInterval,
		"LINK":     cfg.pLink,
		"MIRROR":   cfg.pMirror,
	}); err != nil {
		return err
	}
	return nil
}
