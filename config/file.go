package config

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func (cfg *Config) LoadFile(file string) error {
	f, err := os.Open(file)
	if err != nil {
		return err
	}
	defer f.Close()

	s := bufio.NewScanner(f)
	for s.Scan() {
		if err := cfg.ParseLine(s.Text()); err != nil {
			return err
		}
	}
	return nil
}

func (cfg *Config) ParseLine(line string, context ...string) error {
	fields := strings.Fields(line)

	if !cfg.parser.IsRule(line) {
		return nil
	}

	// Additional context.
	fields = append(context, fields...)
	if err := cfg.p(fields); err != nil {
		return fmt.Errorf("In line %q: %v", line, err)
	}
	return nil
}
