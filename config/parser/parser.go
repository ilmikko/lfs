package parser

import (
	"fmt"
	"strings"
)

type Parser struct{}

func (p *Parser) Unknown(cmd string, help ...string) error {
	if len(help) == 0 {
		return fmt.Errorf("Unknown field: %q", cmd)
	}

	return fmt.Errorf("Unknown field: %q (%s)", cmd, strings.Join(help, "\n"))
}

func (p *Parser) IsRule(line string) bool {
	fields := strings.Fields(line)

	// Empty line.
	if len(fields) == 0 {
		return false
	}

	// Comment.
	if fields[0][0] == COMMENT_RUNE {
		return false
	}

	return true
}

func New() *Parser {
	p := &Parser{}

	return p
}
