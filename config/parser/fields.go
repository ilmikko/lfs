package parser

import (
	"fmt"
	"strings"
)

const (
	COMMENT_RUNE = '#'
)

func (p *Parser) Grab3(fields []string) (string, string, string, error) {
	if err := p.LeftN(fields, 3); err != nil {
		return "", "", "", err
	}
	return fields[0], fields[1], fields[2], nil
}

func (p *Parser) Grab2(fields []string) (string, string, error) {
	if err := p.LeftN(fields, 2); err != nil {
		return "", "", err
	}
	return fields[0], fields[1], nil
}

func (p *Parser) Grab1(fields []string) (string, error) {
	if err := p.Left(fields); err != nil {
		return "", err
	}
	return fields[0], nil
}

func (p *Parser) GrabAll(fields []string) (string, error) {
	if err := p.Left(fields); err != nil {
		return "", err
	}
	return strings.Join(fields, " "), nil
}

func (p *Parser) LeftN(fields []string, n int) error {
	if l := len(fields); l < n {
		return fmt.Errorf("needs additional fields (%d supplied, %d required).", l, n)
	}
	return nil
}

func (p *Parser) Left(fields []string) error {
	if len(fields) == 0 {
		return fmt.Errorf("needs additional fields.")
	}
	return nil
}
