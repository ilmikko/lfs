package config

import (
	"flag"
	"fmt"
)

var (
	flagConfigFile string
)

func (cfg *Config) parseFlags() error {
	flag.StringVar(&flagConfigFile, "config", "", "Path to configuration file.")
	flag.Parse()

	if f := flagConfigFile; f != "" {
		if err := cfg.LoadFile(f); err != nil {
			return fmt.Errorf("LoadConfig(%q): %v", f, err)
		}
	}
	return nil
}
