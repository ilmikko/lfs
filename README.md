![LFS Logo](img/lfs.png)

# Lightweight File Sync

A tool for synchronizing multiple directories based on access times.
LFS is a very simple file synchronisation tool that supports both small and large files of any type.
It is by design fully atomic and resistant to failure when moving data over.
A file transfer within a mirror will always continue regardless of when a LFS process has failed.

The synchronisation algorithm works in such a manner that it always stabilises,
regardless of the amount of steps taken to synchronise all the nodes in an arbitrary network.
This is because **the only axiom to which file to keep is which one has been modified the most recently**.
The algorithm also works atomically even if it is being utilised by multiple LFS processes.
This allows for the possibility of running multiple nodes across multiple machines.

LFS works on a file level, so no complex conflict resolution needed.
If two propagations happen at the same time, the LFS mirrors will choose to propagate the later file.
This does mean that an unsynchronised file can overwrite a synchronised file if it is touched before it has been overwritten.
Keeping this in mind, LFS is a powerful tool for synchronising almost anything across any number of platforms.

## Mirrors

LFS uses a concept called **Mirrors**.
Mirrors can be imagined as the nodes in a network.
Not all mirrors are required to be on the same physical machine.

A simple file synchronisation setup might have two mirrors:

```
Machine 1 [Mirror1] <---> Machine 2 [Mirror2]
```

LFS Mirrors use a custom format of storing file data and metadata.
In order to utilise a mirror, you will usually need to map files in or out (or both) of the mirror.
A file mapping will cause a directory's files to be updated according to the mirror.

### File Mappings

There are three types of mappings: IN, OUT and SYNC.
A SYNC mapping is simply both IN and OUT at the same time.

An IN mapping will listen to changes in a directory, and clone those changes into a mirror.

```
[IN] ----> [Mirror]
```

An OUT mapping will map any changes in a mirror into a directory.

```
[Mirror] ----> [OUT]
```

A SYNC mapping will map changes both ways, thus synchronising the state between a directory and a mirror.

```
[SYNC] <---> [Mirror]
```

Our simple setup could then look as follows:

```
Machine 1 [Mirror1] <---> Machine 2 [Mirror2]
[SYNC1] <---> [Mirror1] (on Machine 1)
[SYNC2] <---> [Mirror2] (on Machine 2)
```

Now, if a file was to be created in the SYNC1 directory on Machine 1, it would propagate all the way through to Machine 2 into the same path.

## Setups

You can have a variety of setups created using LFS.
Do note that you can have any number of mirrors in a single LFS process, and any number of LFS processes on a single machine.
This allows using directory linking between networked machines, and LFS on both of them for synchronisation.

### Single mirror + two ends.

This is the simplest setup, but not very useful as it only contains a single mirror.
It can be used on local machines for synchronising two directories.

```
[SYNC1] <---> [MIRROR1] <---> [SYNC2]
```

### Single mirror + separate r/w ends.

Same setup as the previous example, except files are only transferred one way.

```
[IN] ----> [MIRROR1] ----> [OUT]
```

### Double mirror + two ends.

This is a common setup for synchronising files over multiple machines.
Here state is transferred using two mirrors that are kept in sync.

```
[SYNC1] <---> [MIRROR1] <---> [MIRROR2] <---> [SYNC2]
```

If the processes are split in two, two configurations can be used.
This works identically to the above code block.

```
[SYNC1] <---> [MIRROR1] <---> [MIRROR2]
[SYNC2] <---> [MIRROR2] <---> [MIRROR1]
```

### Triple mirror cycle + 3 ends.

A cyclic mirror setup is not a problem either.
Mirrors will have a metadata cache and will know if they have seen a file before, so no infinite looping will occur.
You can attach any number of mirrors to the network and still have the files synchronised over the entire network.

```
[SYNC1] <---> [MIRROR1] <---> [MIRROR2]
[SYNC2] <---> [MIRROR2] <---> [MIRROR3]
[SYNC3] <---> [MIRROR3] <---> [MIRROR1]
```
