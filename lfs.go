package main

import (
	"log"

	"gitlab.com/ilmikko/lfs/config"
	"gitlab.com/ilmikko/lfs/lfs"
)

func mainErr() error {
	cfg, err := config.New()
	if err != nil {
		return err
	}

	l, err := lfs.New(cfg)
	if err != nil {
		return err
	}

	return l.Watch()
}

func main() {
	if err := mainErr(); err != nil {
		log.Fatalf("Error: %v", err)
	}
}
